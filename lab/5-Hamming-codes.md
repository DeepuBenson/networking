# Hamming codes and its dual.



Recall that the Hamming and Dual Hamming code are codes with parameter
$[2^r -1, 2^r - r - 1, 3]$ and $[2^r -1, r, 2^{r-1}]$
respectively. Given the parameter $r$ as input implement the encoding
decoding procedure for the these codes.


1. Your program should be generic, it should be possible to vary $r$.

2. The input should be taken as a bit vector of appropriate dimension (2^r - r - 1 in the
   case of the Hamming code and $r$ for its dual.

3. The output should be a valid code word.


For ease of checking, you can use the same indexing of bits as
described in the class.
